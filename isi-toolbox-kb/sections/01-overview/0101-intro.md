# This is ISI

ISI stands for **I**KEA **S**upplier **Integration** and is an B2B application in "solution area". Sole purpose is integrating IKEA suppliers with [GPS](link) application

GPS is one of the critical applications in Supplier Execution that takes care of IKEA purchasing. IKEA suppliers communicate with IKEA using EDI standard and hence are also called as EDI suppliers. ISI acts as gateway between GPS and EDI suppliers, it helps transfer information both ways between GPS and EDI suppliers. ISI integrates its EDI suppliers to GPS through AS2 messaging protocol over the Internet via http/https protocols. The supply chain execution is as shown below.