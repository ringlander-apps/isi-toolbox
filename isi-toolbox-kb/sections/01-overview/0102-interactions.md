### ISI Interactions

The interaction of ISI with other system is through GPS. ISI is like a gateway between these applications and the suppliers. The interactions of other system and it dependencies can be shown pictorially as shown below. Interaction of applications with GPS is either via IDRS i.e. IKEA Data Routing System or FTP protocols or AS2 protocol. AS2 is a protocol that the EDIFACT suppliers and ANSIX12 suppliers use to transfer data with ISI.

![Interactions_overview](img/interactions-01.jpg)
(__*Interaction of ISI with other applications*__)

ISI acts as a bridge between GPS and Suppliers. It mainly is used to fetch new orders from the GPS database and forward it to the intended supplier and to receive confirmation for the order from the supplier to IKEA. The top level view of interaction between GPS and EDI/ECIS suppliers is as shown below.

![Interactions02](img/interactions-02.jpg)
(__*ISI Interaction*__)


![Interactions_logical](img/interactions-03.jpg)
(__*Logical Representation of Interaction*__)